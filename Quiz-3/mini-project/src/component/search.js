import { Input, Space } from 'antd';
import { AudioOutlined } from '@ant-design/icons';
import React, { useState} from 'react'
import { Link } from 'react-router-dom';

function SearchBar({ placeholder, data }) {

  const [filteredData, setFilteredData] = useState([]);
  const [wordEntered, setWordEntered] = useState("");

  const handleFilter = (event) => {

    const searchWord = event.target.value

    setWordEntered(searchWord)

    const newFilter = data.filter((value) => {
      return value.name.toLowerCase().includes(searchWord.toLowerCase());
    });

    if (searchWord === "") {
      setFilteredData([]);
    } else {
      setFilteredData(newFilter);
    }
  };

  const { Search } = Input;

  return (
    <div>
      <div className="search">
          <Space direction="vertical">
            <Search placeholder={placeholder} value={wordEntered} onChange={handleFilter} allowClear style={{ width: 200 }} />
          </Space>  
      </div>
      {filteredData.length != 0 && (
        <div className="result">
          {filteredData.slice(0, 15).map((value, key) => {
            return (
              <Link to="/search/:value">
                <p>{value.name}</p>
              </Link>
            );
          })}
        </div>
      )}
    </div>
  );
}

export default SearchBar;
