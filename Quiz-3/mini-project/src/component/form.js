import react, { useContext, useEffect } from "react";
import { MobileContext } from "../context/context";
import { useHistory, useParams } from "react-router";
import { render } from '@testing-library/react'
import { Form, Input, Button, Checkbox, message, InputNumber } from 'antd';


const FormData = () => {

    const {fetchDatabyID, input, setInput, currentId, setCurrentId, functions} = useContext(MobileContext)

    const {functionSubmit, functionUpdate} = functions
    
    let {idData} = useParams()
  
    useEffect(() => {

        if(idData !== undefined){
            fetchDatabyID(idData)
        }
    }, [])

    const submitted = () => {
        render(message.success("Data berhasi ditambahkan!"))
    }

    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name
        
        setInput({...input, [name] : value})
    }

    const handleSubmit = (e)  => {
        e.preventDefault()
        const name = e.target.name
        const category = e.target.category
        if(currentId === null){
            
            functionSubmit()
            
        } else {
          functionUpdate(currentId)    
          setCurrentId(null)
          
          }
          
        
    }

    const onFinish = (values) => {
      console.log('Success:', values);
      submitted()
    };
  
    const onFinishFailed = (errorInfo) => {
      console.log('Failed:', errorInfo);
    };
    
    return (
      <div className="form">
      
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        onSubmitCapture={handleSubmit}
        
      >
        <Form.Item
          
          label="Name"
          name="name"
          rules={[
            {
              required: true,
              message: 'Please input the name!',
            },
          ]} 
        >
          <Input onChange={handleChange} value={input.name} />
        </Form.Item>
  
        <Form.Item
          label="Category"
          name="category"
          onChangeCapture={handleChange}
          rules={[
            {
              required: true,
              message: 'Please input the category',
            },
          ]}
        >
          <Input/>
        </Form.Item>
        <Form.Item
          
          label="Description"
          name="description"
          onChangeCapture={handleChange}
          value={input.description}
          rules={[
            {
              required: true,
              message: 'Please input the description!',
            },
          ]} 
        >
          <Input />
        </Form.Item>
        <Form.Item 
        label="Release Year"
        name="release_year"
        onChangeCapture={handleChange}
          value={input.release_year}
          rules={[
            {
              required: true,
              message: 'Please input release year!',
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item 
        label="Size (MB)"
        name="size"
        onChangeCapture={handleChange}
          value={input.size}
          rules={[
            {
              required: true,
              message: 'Please input Size!',
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item 
        label="Price"
        name="price"
        onChangeCapture={handleChange}
          value={input.price}
          rules={[
            {
              required: true,
              message: 'Please input price!',
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item 
        label="Rating"
        name="rating"
        onChangeCapture={handleChange}
          value={input.rating}
          rules={[
            {
              max: 5,
              required: true,
              message: 'Please input rating!',
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item
          label="Platform"
          name="platform"
          onChangeCapture={handleChange}
          value={input.description}
          rules={[
            {
              required:true,
            },
          ]}
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
          
        >
          <Checkbox>android</Checkbox>
          <Checkbox>IOS</Checkbox>
        </Form.Item>
  
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
      </div>
    );
  };

export default FormData