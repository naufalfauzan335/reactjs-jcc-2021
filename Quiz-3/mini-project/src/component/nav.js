import React, { useContext } from "react";
import '../assets/css/style.css'
import { Link } from "react-router-dom";
import Image from "../assets/img/logo.png";
import { AudioOutlined } from '@ant-design/icons';
import Search from "./search"
import { MobileContext } from '../context/context'

const Nav = () => {

    const {daftar} = useContext(MobileContext)
    return (
        <>
            <nav className="topnav">
                <img src={Image}/>
                    <Link to="/">Home</Link>
                    <Link to="/mobile-list">Movie List</Link>
                    <Link to="/About">About</Link>
                <form>
                    <Search placeholder="Search..." data={daftar}/>
                </form>
            </nav>            
        </>
    )
}

export default Nav