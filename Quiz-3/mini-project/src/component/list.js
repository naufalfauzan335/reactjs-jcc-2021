import React, { useState, useEffect, useContext} from 'react'
import { MobileContext } from '../context/context'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router'
import { Table, Tag, Space, message, Button } from 'antd';
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import { render } from '@testing-library/react'


const Table = () => {
    let history = useHistory()

    const {daftar,fetchStatus, setFetchStatus, functions} = useContext(MobileContext)

    const {fetchData, functionDelete, functionEdit, platform} = functions

    useEffect(() => {

        fetchData()
       
    }, [fetchStatus, setFetchStatus])

    const deleted = () => {
      render(message.success("Data terhapus"))
    }


    const handleEdit = (event) => {
      let idDaftar = parseInt(event.currentTarget.value)
      history.push(`/mobile-form/${idDaftar}`)
      // functionEdit(idDaftar)
    }
    const handleDelete = (event) => {
        let idDaftar = parseInt(event.currentTarget.value)
        
        functionDelete(idDaftar)
        deleted()
    }

    const columns = [
      {
        title: 'No',
        dataIndex:'i',
        key: 'id',
      },  
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Category',
        dataIndex: 'category',
        key: 'category',
      },
      {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
      },
      {
        title: 'Release Year',
        dataIndex: 'release_year',
        key: 'release_year',
      },
      {
        title: 'Rating',
        dataIndex: 'rating',
        key: 'rating',
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
      },
      {
        title: 'Size',
        dataIndex: 'ukuran',
        key: 'ukuran',
      },
      {
        title: 'Platform',
        dataIndex: 'platform',
        key: 'platform',
      },
      {
        title: 'Action',
        key: 'action',
        render: (res, index) => (
          <div key={index}>
            <button onClick={handleEdit} value={res.id} className="btn-Edit"><EditOutlined /></button>
            <button onClick={handleDelete} value={res.id} className="btn-Delete"><DeleteOutlined /></button>
          </div>
        ),
      },
    ];
    
    const data = daftar

    return (
      <div>
        <h1>Mobile App list</h1>
          <Link to="/mobile-form">
            <Button className="btn-add" icon={<PlusOutlined />}>
              Tambahkan data
            </Button>
          </Link>
        <Table columns={columns} dataSource={data} className="Tugas15" />
        
      </div>
      
    )
  }
  
  
export default Table