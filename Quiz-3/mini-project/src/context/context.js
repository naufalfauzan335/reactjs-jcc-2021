import axios from "axios";
import react, {createContext, useState} from "react";
import { useParams } from "react-router";

export const MobileContext = createContext()

export const MobileProvider = props => {

    const [daftar, setDaftar] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)
    const [input, setInput] = useState({
      id : null, 
      name: "",
      category: "",
      description: "",
      release_year: 0,
      rating: 0,
      price: 0,
      size : 0,
      is_android_app: undefined,
      is_ios_app: undefined
    })
    const [currentId, setCurrentId] = useState(null)
  

    const fetchData = async () => {
        let result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
        let data = result.data


        setDaftar(data.map((e) => {
          let platform = dataPlatform(e.is_android_app, e.is_ios_app)  
          let ukuran = dataSize(e.size) 
          let i = 0
            for(i ; id != null; i++){
             i
             }
          let {id,name,category,description, release_year, rating, price, size, is_android_app, is_ios_app} = e
            return {
                id,
                name,
                category,
                description,
                release_year,
                rating,
                price,
                size,
                is_android_app,
                is_ios_app,
                platform,
                ukuran,
                i              
            }
        }))
    }
    if(fetchStatus){
      fetchData()
      setFetchStatus(false)
      
    }


    const fetchDatabyID = async (idData) => {
      let result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idData}`)
      let data = result.data
      setInput({
        id : input.id,
        name : input.name,
        category : input.category,
        description : input.description,
        release_year : input.release_year,
        rating : input.rating,
        price : input.price,
        size : input.size,
        is_android_app : input.is_android_app,
        is_ios_app : input.is_ios_app
      })
      setCurrentId(data.id)
  }

    const functionSubmit = () => {
        axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
            id : input.id,
            name : input.name,
            category : input.category,
            description : input.description,
            release_year : input.release_year,
            rating : input.rating,
            price : input.price,
            size : input.size,
            is_android_app : input.is_android_app,
            is_ios_app : input.is_ios_app})

          .then (() => {
            setFetchStatus(true)
        })
    }

    const functionEdit = (idDaftar) => {
        axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idDaftar}`)

      .then((e) => {

        let data = e.data
        setInput(data)
        setCurrentId(data.id)
      })
    }
    
    const functionDelete = (idDaftar) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${idDaftar}`)
          .then(() => {
          setFetchStatus(true)
        })
    }
    const functionUpdate = (currentId) => {
        axios.put(` http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`, {
            id : input.id,
            name : input.name,
            category : input.category,
            description : input.description,
            release_year : input.release_year,
            rating : input.rating,
            price : input.price,
            size : input.size,
            is_android_app : input.is_android_app,
            is_ios_app : input.is_ios_app})     

          .then (() => {
            setFetchStatus(true)
          })
          setInput({
            id : null, 
            name: "",
            category: "",
            description: "",
            release_year: 0,
            rating: 0,
            price: 0,
            size : 0,
            is_android_app: undefined,
            is_android_app: undefined,
          })
    }

    const dataPlatform = (is_android_app, is_ios_app) => {
        if(is_android_app == true && is_ios_app == true) {
            return("Android & IOS")
        } else if (is_android_app == true && is_ios_app == false){
            return ("Android")
        } else if (is_android_app == false && is_ios_app == true){
            return ("IOS")
        }
    }   

    

    const dataSize = (size) => {
        return (
            size/1000 + " GB"
        )
    }

    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit,
    }

    return (
        <MobileContext.Provider value = {{
            daftar,
            setDaftar,
            fetchStatus,
            setFetchStatus,
            input,
            setInput,
            currentId,
            setCurrentId,
            functions,
            fetchDatabyID,
        }}>
            {props.children}

        </MobileContext.Provider>
    )
}