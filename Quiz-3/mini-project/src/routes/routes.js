import React from "react";
import { 
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"
import Home from "../component/home";
import Nav from "../component/nav";
import Footer from "../component/footer";
import About from "../component/about";
import Table from "../component/list"
import { MobileProvider } from "../context/context";
import Form from "../component/form";
import Search from "../component/search";

const Routes = () => {
    return (
        <MobileProvider>
            <Router>
                <Nav/>
                <div>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/mobile-list" component={Table}/>
                        <Route exact path="/mobile-form" component={Form}/>
                        <Route exact path="/About" component={About}/>
                        <Route exact path="/search/:value" component={Search}/>
                    </Switch>
                </div>
                <Footer/>
            </Router>
        </MobileProvider>
        
    )
}

export default Routes