import React from 'react';
import './assets/css/style.css';
import Routes from './routes/routes'
import 'antd/dist/antd.css';

function App() {
  return (
    <div>
      <Routes/>
    </div>
  );
}

export default App;
