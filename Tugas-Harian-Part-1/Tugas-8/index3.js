var filterBooksPromise = require('./promise2.js')

function execute(){ 
    filterBooksPromise(true, 40)
    .then (function(fullfilled){
       console.log(fullfilled)
    })
    .catch (function(error){
        console.log(error)
    })
}
execute()

async function doAsync (){
    const result = await filterBooksPromise(false, 250)
    console.log(result)
}
doAsync()

async function tryCatch(){
    try {
     const wait = await   filterBooksPromise(true, 30)
     console.log(wait)
    }
    catch(err){
        console.log(err)
    }
}

tryCatch()