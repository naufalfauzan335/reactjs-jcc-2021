// Jawaban Soal 1

var kataPertama = "saya ";
var kataKedua = "senang ";
var kataKetiga = "belajar ";
var kataKeempat = "javascript";
console.log(kataPertama.concat(kataKedua, kataKetiga, kataKeempat));

// Jawaban Soal 2

var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang;
var luasSegitiga;

kelilingPersegiPanjang = 2 * (Number(panjangPersegiPanjang) + Number(lebarPersegiPanjang));
luasSegitiga = (Number(alasSegitiga) * Number(tinggiSegitiga)) / 2;

console.log(kelilingPersegiPanjang);
console.log(luasSegitiga);

// Jawaban Soal 3

var sentences= 'wah javascript itu keren sekali'; 

var firstWord= sentences.substring(0, 3); 
var secondWord= sentences.substring(4, 14);
var thirdWord= sentences.substring(15, 18);
var fourthWord=  sentences.substring(19, 24);
var fifthWord= sentences.substring(25, 31)

console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

// Jawaban Soal 4

var nilaiJohn = 80;
var nilaiDoe = 50;
 
if(nilaiJohn >= 80){
    console.log("Nilai John adalah A")
} else if(nilaiJohn >= 70){
    console.log("nilai John adalah B")
} else if(nilaiJohn >= 60 ){
    console.log("nilai John adalah C")
} else if(nilaiJohn >= 50 ){
    console.log("nilai John adalah D")
} else if(nilaiJohn < 50 ){
    console.log("nilai John adalah E")
}

if(nilaiDoe >= 80){
    console.log("Nilai Doe adalah A")
} else if(nilaiDoe >= 70){
    console.log("Nilai Doe adalah B")
} else if(nilaiDoe >= 60 ){
    console.log("Nilai Doe adalah C")
} else if(nilaiDoe >= 50 ){
    console.log("Nilai Doe adalah D")
} else if(nilaiDoe < 50 ){
    console.log("Nilai Doe adalah E")
}

// Jawaban Soal 5

var tanggal = 5;
var bulan = 1;
var tahun = 2003;

switch(bulan) {
    case 1:{ bulan = 'Januari'; break;}
    case 2:{ bulan = 'Februari'; break;}
    case 3:{ bulan = 'Maret'; break;}
    default: {break;}
}
console.log(tanggal + ' ' + bulan + ' ' + tahun)

