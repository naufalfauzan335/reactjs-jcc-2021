// Jawaban Soal 1

console.log('Looping Pertama')

var i = 2
while(i <= 20){
    console.log( i + ' - I love coding')
i +=2
}

console.log('Looping Kedua')

var i = 20
while(i >= 2){
    console.log( i + ' - I will become a frontend developer')
i -=2
}

// Jawaban Soal 2 

var i = 1
x3 = 3
for( i; i <= 20; i++){
    if((i % 2 == 1) && (i % x3 ==0)){
        console.log(i + ' - I love coding')
    } else if (i % 2 == 0){
        console.log(i + ' - Berkualitas')
    } else if (i % 2 == 1){
        console.log(i + ' - Santai')
    }
}

// Jawaban Soal 3

var i = '#' 
for( i; i.length <= 7; i += '#'){
    console.log(i)
}

// Jawaban Soal 4

var kalimat = ["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]

kalimat.shift()
kalimat.splice(2,1)

var join = kalimat.join(' ')
console.log(join)

// Jawaban Soal 5

var sayuran=[]
sayuran.splice(0, 0, 'Kangkung', 'Bayam', 'Buncis', 'Kubis', 'Timun', 'Seledri', 'Tauge')
sayuran.sort()

for( var i = 0; i < sayuran.length; i++){
    console.log( i + 1 + '.' + sayuran[i])
}