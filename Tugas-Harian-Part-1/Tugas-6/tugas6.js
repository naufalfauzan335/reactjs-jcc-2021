// Jawaban soal 1
console.log("----Jawaban 1----")

let luasLingkaran = (r, phi = 22/7) => {
    return (phi *  r * r )
}

let kelilingLingkaran = ( r, phi = 22/7) => {
    return (phi * 2 * r)
}
console.log(luasLingkaran(7))
console.log(kelilingLingkaran(7))

// Jawaban soal 2
console.log("----Jawaban 2----")


let introduce = (...rest) => {

    let [nama, umur, gender, pekerjaan] = rest
    
    if (gender === "Laki-Laki") {
        panggilan = "Pak"
    } else if (gender === "Perempuan"){
        panggilan = "Bu"
    }

    return `${panggilan} ${nama} adalah seorang ${pekerjaan} yang berusia ${umur} tahun`
}
//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

// Jawaban soal 3
console.log("----Jawaban 3----")


const newFunction = (firstName, lastName) => {
    return {
        firstName : firstName,
        lastName : lastName,
        fullName : fullName => console.log(`${firstName} ${lastName}`)
    }
}

//kode di bawah ini jangan dirubah atau dihapus
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()

// Jawaban soal 4
console.log("----Jawaban 4----")

let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 
const { name : phoneName, brand : phoneBrand, year, colors } = phone
const [colorBronze, colorWhite, colorBlack] = colors


 // kode di bawah ini jangan dirubah atau dihapus
 console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 


//  Jawaban soal 5
console.log("----Jawaban 5----")

let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

buku = {
    ...buku, 
    ... dataBukuTambahan, 
    warnaSampul : [...buku.warnaSampul, ...warna]
}

console.log(buku)