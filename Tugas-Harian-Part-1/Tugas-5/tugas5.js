// Jawaban Soal 1
console.log('----Jawaban 1----')
function luasPersegiPanjang (a, b){
    return a * b
}

function kelilingPersegiPanjang(a, b){
    return 2*(a + b)
}
function volumeBalok(a, b, c){
    return a * b * c
}


var panjang= 12
var lebar= 4
var tinggi = 8
 
var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
var volumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(luasPersegiPanjang) 
console.log(kelilingPersegiPanjang)
console.log(volumeBalok)

// Jawaban Soal 2
console.log('----Jawaban 2----')
function introduce(){
    return 'Nama saya ' + name + ', umur saya ' + age + ', alamat saya di ' + address + ', dan saya punya hobi yaitu ' + hobby + '!'
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 


// Jawaban Soal 3
console.log('----Jawaban 3----')
var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
var objectDaftarPeserta = {
    nama : arrayDaftarPeserta[0],
    jenisKelamin : arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    tahunLahir : arrayDaftarPeserta[3]
}
console.log(objectDaftarPeserta)

// Jawaban Soal 4
console.log('----Jawaban 4----')
var buah = [
    {nama : 'Nanas', warna : 'Kuning', adaBijinya: false, harga : 9000 },
    {nama : 'Jeruk', warna : 'Oranye', adaBijinya: true, harga : 8000 },
    {nama : 'Semangka', warna : 'Hijau & Merah', adaBijinya: true, harga : 10000 },
    {nama : 'Pisang', warna : 'kuning', adaBijinya: false, harga : 5000 }
]
var buahFilter = buah.filter(function(item){
    return item.adaBijinya != true;
 })
 
 console.log(buahFilter)

// Jawaban Soal 5
console.log('----Jawaban 5----')
function tambahDataFilm(a, b, c, d){
    
    objectDataFilm = {
        nama : a,
        durasi : b,
        genre : c,
        tahun : d
    }
    dataFilm.push(objectDataFilm)
    return dataFilm
}

var dataFilm = []

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")

console.log(dataFilm)