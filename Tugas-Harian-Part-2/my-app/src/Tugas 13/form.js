import react, { useContext } from "react";
import { MahasiswaContext } from "../context/context";
import '../assets/css/TableForm.css'
const Form = () => {

    const {input, setInput, currentId, setCurrentId, functions} = useContext(MahasiswaContext)

    const {functionSubmit, functionUpdate} = functions


    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name
   
        setInput({...input, [name] : value})
       }

    const handleSubmit = (e)  => {
        e.preventDefault()
        
        if(currentId === null){
            
            functionSubmit()
          
        } else {
          functionUpdate(currentId)    
          setCurrentId(null)
          }
      }

    return (
        <>  
            <div className="container">
            
            <form onSubmit={handleSubmit} >
            <h2>Form Data Baru</h2>
                        <strong>Nama :</strong>
                        <br />
                        <input className='input' onChange={handleChange} value={input.name} type="text" name='name' required/>
                        <br />
                        <strong>Mata Kuliah :</strong>
                        <br />
                        <input className='input' onChange={handleChange} value={input.course} type="text" name='course' required />      
                        <br />
                        <strong>Nilai :</strong>
                        <br />
                        <input className='input' onChange={handleChange} value={input.score} type="number" name="score" required min = {0} max={100} /> 
                        <br />
                <button className='btn'>Submit</button>
            </form>
            </div>
            
        </>
    )
}

export default Form