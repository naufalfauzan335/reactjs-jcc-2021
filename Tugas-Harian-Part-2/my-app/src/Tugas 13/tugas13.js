import react from "react";
import { MahasiswaProvider } from "../context/context";
import Table from "./table";
import Form from "./form";
import '../assets/css/TableForm.css'

const Mahasiswa = () => {
    
    return (
        <MahasiswaProvider>
            <Table/>
            <Form/>
        </MahasiswaProvider>
        
    )

}

export default Mahasiswa