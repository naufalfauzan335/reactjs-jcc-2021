
import '../assets/css/tugas9.css'
import image from '../assets/img/logo.png'


const List = (props) => {
  return <div class="input-checkbox">
    <input class="checkbox" type="checkbox" name="" id="checkbox" />
    <label htmlFor="checkbox">{props.list}</label>
  </div>
}


function App() {
  return (
    <>
    <div className="card">
      <img src={image} alt="" />

      <div className="title">
        <h1 >THINGS TO DO</h1>
        <p>During Bootcamp in jabar codingcamp</p>
      </div>

    
      <div className = "main">
        <List list = "Belajar GIT & CU"/>
        <List list = "Belajar HTML & CSS"/>
        <List list = "Belajar Javascript"/>
        <List list = "Belajar ReactJS Dasar"/>
        <List list = "Belajar ReactJS Advance"/>
      </div>

      <div className = "send">
        <div>
          <button class = "submit" type = "submit">Send</button>
        </div>
      </div>
      
    </div>
    </>
  );
}

export default App;

