import React from "react"
import { MahasiswaProvider } from "../context/context";
import { ThemeProvider } from "../context/themeContext";
import { 
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"
import Tugas9 from "../Tugas 9/tugas9"
import Tugas10 from "../Tugas 10/tugas10"
import Tugas11 from "../Tugas 11/tugas11"
import Tugas12 from "../Tugas 12/tugas12"
import Tugas13 from "../Tugas 13/tugas13"
import Table14 from "./table"
import Form from "./form"
import Table15 from "../Tugas 15/Tugas15List";
import Form15 from "../Tugas 15/Tugas15Form";


import Nav from "./nav"


const Routes = () => {
    return (
        <>

            
            <Router>
                <ThemeProvider>
                <Nav/>
                <div>
                    <Switch>
                        <Route exact path="/" component={Tugas9}/>
                        <Route exact path="/Tugas10" component={Tugas10}/>
                        <Route exact path="/Tugas11" component={Tugas11}/>
                        <Route exact path="/Tugas12" component={Tugas12}/>
                        <Route exact path="/Tugas13" component={Tugas13}/>
                        
                        <Route exact path="/Table14" component={Table14}/>
                        <Route exact path="/Form" component={Form}/>
                        <Route exact path="/Form/:idData" component={Form}/>

                        <Route exact path="/Table15" component={Table15}/>
                        <Route exact path="/Form15" component={Form15}/>
                        <Route exact path="/Form15/:idData" component={Form15}/>
                        
                    </Switch>
                </div>
                
                </ThemeProvider>
            </Router>

            
            
        </>
    )
}

export default Routes