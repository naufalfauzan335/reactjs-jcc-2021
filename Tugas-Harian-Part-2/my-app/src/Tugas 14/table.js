import React, { useState, useEffect, useContext} from 'react'
import axios from 'axios'
import { MahasiswaContext } from '../context/context'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router'


const Table = () => {
    let history = useHistory()

    const {daftar,fetchStatus, setFetchStatus, functions} = useContext(MahasiswaContext)

    const {fetchData, functionDelete, functionEdit, indexNilai} = functions

    useEffect(() => {

        fetchData()
       
    }, [fetchStatus, setFetchStatus])

    const handleEdit = (event) => {
      let idDaftar = parseInt(event.target.value)
      history.push(`/Form/${idDaftar}`)
      // functionEdit(idDaftar)
    }
    const handleDelete = (event) => {
        let idDaftar = parseInt(event.target.value)
        
        functionDelete(idDaftar)

    }

    


    return (
      <div className="container-1">
        <h1>Daftar Mahasiswa</h1>
        <table>
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Mata Kuliah</th>
                      <th>Nilai</th>
                      <th>Indeks</th>
                      <th>Aksi</th>
                  </tr>
              </thead>
              <tbody>
                {
                  daftar !== null && (
                    <>
                      {
                        daftar.map((e, index)=>{
                          return (
                            <tr key= {index}>
                                <td>{index + 1}</td>
                                <td>{e.name}</td>
                                <td>{e.course}</td>
                                <td>{e.score}</td>
                                <td>{indexNilai(e.score)}</td>
                                <td>
                                    <button onClick={handleEdit} value={e.id} className="btn-edit">Edit</button>
                                    <button onClick={handleDelete} value={e.id} className="btn-delete">Delete</button>
                                </td>
                            </tr>
                      )
                  }) 
                  }
                    </>
                  )
                }

                  
              </tbody>
          </table>
          <div>
          <Link to="/Form">
            <button className="add">
              Tambahkan data
            </button>
          </Link>
          </div>
          
            
          
      </div>
    )
  }
  
  
export default Table