import react, { useContext } from "react";
import { ThemeContext } from "../context/themeContext";
import { Switch } from 'antd';

const ButtonSwitch = () => {
    const {theme, setTheme} = useContext(ThemeContext)
      
    const handleButton = () => {
        setTheme(theme === "light" ? "dark" : "light")
    }
    return(

        <div>
             <Switch checkedChildren="Light" unCheckedChildren="Dark" defaultChecked onChange={handleButton} />
        </div>
    )
}

export default ButtonSwitch