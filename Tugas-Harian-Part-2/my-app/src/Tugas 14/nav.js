import React, { useContext } from "react";
import { Link } from "react-router-dom";
import '../assets/css/nav.css'
import { ThemeContext } from "../context/themeContext";
import ButtonSwitch from "./button"

const Nav = () => {
    
    const {theme, setTheme} = useContext(ThemeContext)
    const styleNav = theme === "light" ? "light" : "dark"

    return (
        <div>

            <nav className={`nav ${styleNav}`}>
            <ul className="nav-ul">
                <li>
                    <Link to="/"> Tugas 9</Link>
                </li>
                <li>
                    <Link to="/Tugas10"> Tugas 10</Link>
                </li>
                <li>
                    <Link to="/Tugas11"> Tugas 11</Link>
                </li>
                <li>
                    <Link to="/Tugas12"> Tugas 12</Link>
                </li>
                <li>
                    <Link to="/Tugas13"> Tugas 13</Link>
                </li>
                <li>
                    <Link to="/Table14"> Tugas 14</Link>
                </li>
                <li>
                    <Link to="/Table15"> Tugas 15</Link>
                </li>
                <li>
                    <ButtonSwitch/>
                </li>
            </ul>
        </nav>
        </div>
    )
}
export default Nav