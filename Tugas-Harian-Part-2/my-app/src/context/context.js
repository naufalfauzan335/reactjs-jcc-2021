import axios from "axios";
import react, {createContext, useState} from "react";
import { useParams } from "react-router";

export const MahasiswaContext = createContext()

export const MahasiswaProvider = props => {

    const [daftar, setDaftar] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)
    const [input, setInput] = useState({
      name: "",
      course: "",
      score: 0
    })
    const [currentId, setCurrentId] = useState(null)
  

    const fetchData = async () => {
        let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
        let data = result.data

        setDaftar(data.map((e) => {
          let indexScore = indexNilai(e.score)
          let {id,name,course,score} = e

            return {
                id,
                name,
                course,
                score,
                indexScore
            }
        }))
    }
    if(fetchStatus){
      fetchData()
      setFetchStatus(false)
      
    }

    const fetchDatabyID = async (idData) => {
      let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
      let data = result.data
      setInput({
          id : data.id,
          name : data.name,
          course : data.course,
          score: data.score,
      })
      setCurrentId(data.id)
  }

    const functionSubmit = () => {
        axios.post(`http://backendexample.sanbercloud.com/api/student-scores$`, {
            name : input.name, 
            course : input.course, 
            score : input.score })

          .then (() => {
            setFetchStatus(true)
        })
    }

    const functionEdit = (idDaftar) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idDaftar}`)

      .then((e) => {

        let data = e.data
        setInput(data)
        setCurrentId(data.id)
      })
    }
    
    const functionDelete = (idDaftar) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idDaftar}`)
          .then(() => {
          setFetchStatus(true)
        })
    }
    const functionUpdate = (currentId) => {
        axios.put(` http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {
            name : input.name, 
            course : input.course, 
            score : input.score})

          .then (() => {
            setFetchStatus(true)
          })
          setInput({
            name: "",
            course: "",
            score: 0
          })
    }

    const indexNilai = (score) => {
        if(score >= 80 ){
          return ('A')
        } else if (score >= 70 && score < 80){
          return ('B')
        } else if (score >= 60 && score < 70 ){
          return ('C')
        } else if (score >= 50 && score < 60){
          return ('D')
        } else if ( score < 50){
          return ('E')
        }
      }

    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit,
        indexNilai
    }

    return (
        <MahasiswaContext.Provider value = {{
            daftar,
            setDaftar,
            fetchStatus,
            setFetchStatus,
            input,
            setInput,
            currentId,
            setCurrentId,
            functions,
            fetchDatabyID,
        }}>
            {props.children}

        </MahasiswaContext.Provider>
    )
}