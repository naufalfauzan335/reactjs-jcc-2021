import React, {useState} from 'react'
import '../assets/css/TableForm.css'


const App = () => {

  var daftarBuah = [
    {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
    {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
    {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
    {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
    {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
  ]
  
  const [daftar, setDaftar] = useState(daftarBuah)
  const emptyForm = { nama: '', hargaTotal : '', beratTotal : '' }
  const [buah, setBuah] = useState("")
  const [currentIndex, setCurrentIndex] = useState(-1)

  const handleChange = (event) => {
    
    const { name, value } = event.target
    setBuah({ ...buah, [name]: value })
  }

  const handleSubmit = (event)  => {
    event.preventDefault()
    let newData = daftar
    
      if ((currentIndex === -1) && (buah.beratTotal >= 2000) && (buah.nama != '') && (buah.hargaTotal != 0)){
        newData = [...daftar, buah]
      } else {
        newData[currentIndex] = buah
       }

      if (buah.beratTotal<2000 && buah.nama != '' && buah.hargaTotal != null ){
        alert('Berat buah Tidak Cukup')
      } else if (buah.beratTotal == 0 && buah.nama == '' && buah.hargaTotal == 0){
        alert("Masukan data terlebih dahulu")
      }
       setDaftar(newData)
       setBuah(emptyForm)
       setCurrentIndex(-1)
  }

  const handleDelete = (event) => {
    let index = parseInt(event.target.value)
    let deletedItem = daftar[index]
    let newData = daftar.filter((e) => {return e !== deletedItem})
    console.log(newData)
    setDaftar(newData)
  }

  const handleEdit = (event) => {
    let index = parseInt(event.target.value)
    let editValue = daftar[index]
    setBuah(editValue)
    setCurrentIndex(event.target.value)
  }
  

  return (
    <div className="container">
      <h1>Daftar Harga Buah</h1>
      <table>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga Total</th>
                    <th>Berat Total</th>
                    <th>Harga per Kg</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {
                daftar.map((daftar, index)=>{
                    return (
                        <tr key= {index}>
                            <td>{index + 1}</td>
                            <td>{daftar.nama}</td>
                            <td>{daftar.hargaTotal}</td>
                            <td>{daftar.beratTotal/1000} Kg</td>
                            <td>{daftar.hargaTotal/(daftar.beratTotal/1000)}</td>
                            <td>
                                <button onClick={handleEdit} value={index} className="btn-edit">Edit</button>
                                <button onClick={handleDelete} value={index} className="btn-delete">Delete</button>
                            </td>
                        </tr>
                    )
                }) 
                }
            </tbody>
        </table>

          <h1>Form Daftar Harga Buah</h1>

        <form onSubmit={handleSubmit } >

                  <strong >Nama :</strong>
                  <br />
                  <input className='input' onChange={handleChange} value={buah.nama} type="text" name='nama'/>
                  <br />
                  <strong >Harga Total:</strong>
                  <br />
                  <input className='input' onChange={handleChange} value={buah.hargaTotal} type="text" name='hargaTotal' /> 
                  <br />
                  <strong >Berat Total (gram):</strong>
                  <br />
                  <input className='input' onChange={handleChange} value={buah.beratTotal} type="text" name="beratTotal" /> 
                <br />
          <button className='btn'>Submit</button>
        </form>
        
    </div>
  )
}

export default App