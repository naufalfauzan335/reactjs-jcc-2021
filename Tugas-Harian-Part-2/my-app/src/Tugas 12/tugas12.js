import React, {useState, useEffect} from 'react'
import axios from 'axios'
import '../assets/css/TableForm.css'

const App = () => {

    const [daftar, setDaftar] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)
    const [input, setInput] = useState({
      name: "",
      course: "",
      score: 0
    })
    const [currentId, setCurrentId] = useState(null)
    
    useEffect(() => {

        const fetchData = async () => {
            let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            let data = result.data

            setDaftar(data.map((e) => {
              let {id,name,course,score} = e

                return {
                    id,
                    name,
                    course,
                    score,
                }
            }))
        }
        if(fetchStatus){
          fetchData()
          setFetchStatus(false)
          
        }

        
    }, [fetchStatus, setFetchStatus])

    const handleChange = (e) => {
     let value = e.target.value
     let nama = e.target.name

     setInput({...input, [nama] : value})
    }
  
    const handleSubmit = (e)  => {
      e.preventDefault()
      
      let name = input.name
      let course = input.course
      let score = input.score 
      
      if(currentId === null){
        axios.post(`http://backendexample.sanbercloud.com/api/student-scores$`, {name, course, score})
          .then (() => {
            setFetchStatus(true)
        })
      } else axios.put(` http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {name, course, score})
          .then (() => {
            setFetchStatus(true)
          })
          setInput({
            name: "",
            course: "",
            score: 0
          })
          setCurrentId(null)
    }
    const handleEdit = (event) => {
      let idDaftar = parseInt(event.target.value)
      axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idDaftar}`)

      .then((e) => {

        let data = e.data
        setInput(data)
        setCurrentId(data.id)
      })
    }
    const handleDelete = (event) => {
        let idDaftar = parseInt(event.target.value)
          axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idDaftar}`)
          .then(() => {
          setFetchStatus(true)
        })

    }
  
    
    
    const indexNilai = (e) => {
      if(e >= 80 ){
        return ('A')
      } else if (e >= 70 && e < 80){
        return ('B')
      } else if (e >= 60 && e < 70 ){
        return ('C')
      } else if (e >= 50 && e < 60){
        return ('D')
      } else if ( e < 50){
        return ('E')
      }
    }

    return (
      <div className="container">
        <h1>Daftar Mahasiswa</h1>
        <table>
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Mata Kuliah</th>
                      <th>Nilai</th>
                      <th>Indeks</th>
                      <th>Aksi</th>
                  </tr>
              </thead>
              <tbody>
                {
                  daftar !== null && (
                    <>
                      {
                        daftar.map((e, index)=>{
                          return (
                            <tr key= {index}>
                                <td>{index + 1}</td>
                                <td>{e.name}</td>
                                <td>{e.course}</td>
                                <td>{e.score}</td>
                                <td>{indexNilai(e.score)}</td>
                                <td>
                                    <button onClick={handleEdit} value={e.id} className="btn-edit">Edit</button>
                                    <button onClick={handleDelete} value={e.id} className="btn-delete">Delete</button>
                                </td>
                            </tr>
                      )
                  }) 
                  }
                    </>
                  )
                }

                  
              </tbody>
          </table>
  
            
  
          <form onSubmit={handleSubmit} >
          <h2>Form Data Baru</h2>

                    <strong>Nama :</strong>
                    <br />
                    <input className='input' onChange={handleChange} value={input.name} type="text" name='name' />
                    <br />
                    <strong>Mata Kuliah :</strong>
                    <br />
                    <input className='input' onChange={handleChange} value={input.course} type="text" name='course' />      
                    <br />
                    <strong>Nilai :</strong>
                    <br />
                    <input className='input' onChange={handleChange} value={input.score} type="number" name="score" required min = {0} max={100} /> 
                    <br />
            <button className='btn'>Submit</button>
          </form>
          
      </div>
    )
  }
  
  
export default App