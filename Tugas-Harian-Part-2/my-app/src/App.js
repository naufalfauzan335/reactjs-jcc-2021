import React from "react"
import { MahasiswaProvider } from "./context/context"
import Routes from './Tugas 14/routes'
import 'antd/dist/antd.css';

const App = () => {
    return (
        <>
            <MahasiswaProvider>
            <Routes/>
            </MahasiswaProvider>
            
        </>
    )
}

export default App

