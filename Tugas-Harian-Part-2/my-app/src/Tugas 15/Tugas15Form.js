import react, { useContext, useEffect } from "react";
import { MahasiswaContext } from "../context/context";
import '../assets/css/TableForm.css'
import { Link } from "react-router-dom";
import { useHistory, useParams } from "react-router";
import { Table, Tag, Space, message } from 'antd';
import { render } from '@testing-library/react'


const Form15 = () => {

    const {fetchDatabyID, input, setInput, currentId, setCurrentId, functions} = useContext(MahasiswaContext)

    const {functionSubmit, functionUpdate} = functions
    
    let {idData} = useParams()
  
    useEffect(() => {

        if(idData !== undefined){
            fetchDatabyID(idData)
        }
    }, [])

    const submitted = () => {
        render(message.success("Data berhasi ditambahkan!"))
    }

    const handleChange = (e) => {

        let value = e.target.value
        let name = e.target.name
        
        setInput({...input, [name] : value})
    }

    const handleSubmit = (e)  => {
        e.preventDefault()
        
        if(currentId === null){
            
            functionSubmit()
          
        } else {
          functionUpdate(currentId)    
          setCurrentId(null)
          }
        
        submitted()
    }

    return (
        <>  
            <div className="container">
            
            <form onSubmit={handleSubmit} >
            <h2>Form Data Baru</h2>
                        <strong>Nama :</strong>
                        <br />
                        <input className='input' onChange={handleChange} value={input.name} type="text" name='name' required/>
                        <br />
                        <strong>Mata Kuliah :</strong>
                        <br />
                        <input className='input' onChange={handleChange} value={input.course} type="text" name='course' required />      
                        <br />
                        <strong>Nilai :</strong>
                        <br />
                        <input className='input' onChange={handleChange} value={input.score} type="number" name="score" required min = {0} max={100} /> 
                        <br />
                
                <button className='btn'>Submit</button>
            </form>
            </div>
            
        </>
    )
}

export default Form15