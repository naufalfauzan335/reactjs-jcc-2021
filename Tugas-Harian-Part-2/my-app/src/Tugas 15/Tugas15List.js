import React, { useState, useEffect, useContext} from 'react'
import axios from 'axios'
import { MahasiswaContext } from '../context/context'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router'
import { Table, Tag, Space, message, Button } from 'antd';
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import { render } from '@testing-library/react'


const Table15 = () => {
    let history = useHistory()

    const {daftar,fetchStatus, setFetchStatus, functions} = useContext(MahasiswaContext)

    const {fetchData, functionDelete, functionEdit, indexNilai} = functions

    useEffect(() => {

        fetchData()
       
    }, [fetchStatus, setFetchStatus])

    const deleted = () => {
      render(message.success("Data terhapus"))
    }


    const handleEdit = (event) => {
      let idDaftar = parseInt(event.currentTarget.value)
      history.push(`/Form15/${idDaftar}`)
      // functionEdit(idDaftar)
    }
    const handleDelete = (event) => {
        let idDaftar = parseInt(event.currentTarget.value)
        
        functionDelete(idDaftar)
        deleted()
    }

    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Mata Kuliah',
        dataIndex: 'course',
        key: 'course',
      },
      {
        title: 'Nilai',
        dataIndex: 'score',
        key: 'score',
      },
      {
        title: 'Index Nilai',
        dataIndex: 'indexScore',
        key: 'indexScore',
      },
      {
        title: 'Action',
        key: 'action',
        render: (res, index) => (
          <div key={index}>
            <button onClick={handleEdit} value={res.id} className="btn-Edit"><EditOutlined /></button>
            <button onClick={handleDelete} value={res.id} className="btn-Delete"><DeleteOutlined /></button>
          </div>
        ),
      },
    ];
    
    const data = daftar

    return (
      <div>
        <h1>Daftar Mahasiswa</h1>
        <Link to="/Form15">
            <Button className="btn-add" icon={<PlusOutlined />}>
              Tambahkan data
            </Button>
          </Link>
        <Table columns={columns} dataSource={data} className="Tugas15" />
        
      </div>
      
    )
  }
  
  
export default Table15