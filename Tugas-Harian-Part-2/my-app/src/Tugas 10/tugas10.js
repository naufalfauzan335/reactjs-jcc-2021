
import '../assets/css/tugas10.css';
import { useState, useEffect } from 'react';

const Clock = () => {
    const [date, setDate] = useState(new Date())

    useEffect(() => {
        var timer = setInterval (() => time(),1000 )
        return function cleanup() {
            clearInterval(timer)
        }
    })

    function time(){
        setDate(new Date())
    }

    const [count, setcount] = useState(100)

    useEffect(() => {
        if (count !== 0){
            setTimeout(() => {
                setcount(count - 1)
            }, 1000);
        }
    })

    const renderCondition = () => {
        if (count !== 0){
            return (
            <div >
                <h1 className='h1'>Now At - {date.toLocaleTimeString()}</h1>
                <h2>Countdown : {count}</h2>
            </div>
            )
        }
    }

    return (
        <>
            <div className="countdown">
                {renderCondition()}
            </div>
        </>
    )
}

export default Clock