import React, { useContext, useEffect } from "react";
import {  Image } from 'antd';
import { useParams } from "react-router";
import { WebContext } from "../Context/Context";
import { Typography } from 'antd';

const { Title } = Typography;

const GameDetail = () => {
    
    const { dataGame, fetchGamebyID} = useContext(WebContext)

    let {idgame} = useParams()

    useEffect(() => {
        if(idgame !== undefined){
            fetchGamebyID(idgame)
        }
        
    }, [])

    const Player = (a,b) => {
      if(a === 1 || b === 1){
        return "Single Player & Multi Player"
      } else if(a === 1 || b === 0){
        return "Single Player"
      } else if(a === 0 || b === 1){
        return "Multi Player"
      }
    }

    function ImageDetail() {
        return (
          <Image
            width={400}
            src={dataGame.image_url}
          />
        );
      }
    return(
        <>
        
        <div className="detail-game">
            <ImageDetail className="img-detail" />
            <div className="description">
               <Typography>
                    <Title>{dataGame.name} ({dataGame.release})</Title>
                    <Title level={5}> {dataGame.platform} </Title>
                    <Title level={5}> Genre : {dataGame.genre} </Title>
                    <Title level={5}> Player Mode: {Player(dataGame.singlePlayer, dataGame.multiPlayer)} </Title>
                    
               </Typography>
            </div>
        </div>
        </>
    )
}

export default GameDetail