import axios from 'axios';
import { useHistory } from 'react-router-dom';
import {
  Form,
  Input,
  Button,
  message,
} from 'antd';
import { render } from '@testing-library/react'

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RegistrationForm = () => {
  const [form] = Form.useForm();

  let history = useHistory()

  const onFinish = (event) => {
    console.log('Received values of form: ', event);
    axios.post(`https://backendexample.sanbersy.com/api/register`, {
        name : event.name,
        email : event.email,
        password : event.password
    }) .then((e) => {
        render(message.success("Registrasi telah berhasil!"))
        history.push("/login")
    }) .catch((e) => {
        render(message.error("Error, try another account or password"))
    })

  };

  
  return (
    <div className="bg-register">
      <div className="form-register">
      <h1 className="header-form">Register your account</h1>
        <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        method="post"
        >
        <Form.Item
            name="name"
            label="Name"
            tooltip="What do you want others to call you?"
            rules={[
            {
                required: true,
                message: 'Please input your name!',
                whitespace: true,
            },
            ]}
        >
            <Input />
        </Form.Item>  
        <Form.Item
            name="email"
            label="E-mail"
            rules={[
            {
                type: 'email',
                message: 'The input is not valid E-mail!',
            },
            {
                required: true,
                message: 'Please input your E-mail!',
            },
            ]}
        >
            <Input />
        </Form.Item>

        <Form.Item
            name="password"
            label="Password"
            rules={[
            {
                required: true,
                message: 'Please input your password!',
            },
            ]}
            hasFeedback
        >
            <Input.Password />
        </Form.Item>

        <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
            {
                required: true,
                message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
                validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                }

                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
            }),
            ]}
        >
            <Input.Password />
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
            Register
            </Button>
        </Form.Item>
        </Form>
    </div>
    </div>
    
  );
};

export default RegistrationForm