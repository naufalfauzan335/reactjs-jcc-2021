import React, { useContext, useEffect } from "react";
import {  Image } from 'antd';
import { useParams } from "react-router";
import { WebContext } from "../Context/Context";
import { Typography } from 'antd';
import {ClockCircleOutlined} from '@ant-design/icons';
const { Title, Paragraph, } = Typography;

const MovieDetail = () => {
    
    const { dataMovie, fetchMoviebyID} = useContext(WebContext)

    let {Id} = useParams()

    useEffect(() => {
        if(Id !== undefined){
            fetchMoviebyID(Id)
        }
        
    }, [])


    function ImageDetail() {
        return (
          <Image
            width={400}
            src={dataMovie.image_url}
          />
        );
      }
    return(
        <>
        
        <div className="detail">
            <ImageDetail className="img-detail" />
            <div className="description">
               <Typography>
                    <Title>{dataMovie.title} ({dataMovie.year})</Title>
                    <Title level={5}> <ClockCircleOutlined /> {dataMovie.duration}  minutes</Title>
                    <Title level={5}> Genre : {dataMovie.genre} </Title>
                    <Title level={5}> Rating :  {dataMovie.rating}/10 </Title>
                    <Title level={5}> Synopsis </Title>
                    <Paragraph>{dataMovie.description}</Paragraph>
                    <Title level={5}> Review </Title>
                    <Paragraph>{dataMovie.review}</Paragraph>
               </Typography>
            </div>
        </div>
        </>
    )
}

export default MovieDetail