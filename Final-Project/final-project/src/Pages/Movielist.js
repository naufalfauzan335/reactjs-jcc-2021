import React, { useContext, useEffect, useState } from 'react';
import { Table, Space, Tooltip, Input } from 'antd';
import { MovieContext} from '../Context/MovieContext';
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { Button} from 'antd';
import axios from 'axios';

const MovieList = () => {

  const {functions, setInput, state, setDataMovie} = useContext(MovieContext)
  const {fetchData, functionDelete} = functions
  let {fetchStatus,setFetchStatus, dataMovie} = state
  let history = useHistory()
  const { Search } = Input;

  const [filter, setFilter] = useState({
    genre: "",
    year: 0,
    rating: 0
  })

  useEffect(() => {

      if (fetchStatus) {
          fetchData()
          setFetchStatus(false)
      }


  }, [fetchData, fetchStatus, setFetchStatus])

  const handleChangeFilter = (e) => {
    let valueType = e.target.value
    let name = e.target.name

    setFilter({ ...filter, [name] : valueType})
}

  const handleEdit = (event) => {
      let idData = parseInt(event.currentTarget.value)
      history.push(`movie/form/${idData}`)
  }
  const handleDelete = (event) => {
      let idData = parseInt(event.currentTarget.value)
      functionDelete(idData)
  }

  const handleFilter = (e) => {
    e.preventDefault()

    let fetchFilter = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let data = result.data

        let filterData = data.filter((e) => {
            return e.genre === filter.genre || e.year === filter.year || e.rating === filter.rating
        })
        setDataMovie(
          filterData.map((res) => {
              let {id,title,genre,duration,description,image_url,review,year,rating} = res
              return {
                 id,
                 title,
                 genre,
                 duration,
                 description,
                 image_url,
                 review,
                 year,
                 rating,

              }

          })
      )

    }

    fetchFilter()
}

  const onSearch = value => {
    let fetchSearch = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let data = result.data

        let filterResult = data.filter((e) => {
           return Object.values(e).join("").toLowerCase().includes(value.toLowerCase())
        })
        setDataMovie(
          filterResult.map((res) => {
              let {id,title,genre,duration,description,image_url,review,year,rating} = res
              return {
                 id,
                 title,
                 genre,
                 duration,
                 description,
                 image_url,
                 review,
                 year,
                 rating,

              }

          })
      )

    }
    fetchSearch()      
  }
  const handleCreate = () => {
    history.push("/movie/create")
    setInput({
      title : "",
      genre : "",
      duration: 0,
      description : "",
      image_url: "",
      review: "",
      year : 0,
      rating : 0,
      id :0
    })
}  
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title'
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            ellipsis: {
                showTitle: false,
              },
              render: description => (
                <Tooltip placement="topLeft" title={description}>
                  {description}
                </Tooltip>
              ),
        },
        {
          title: 'Image',
          dataIndex: 'image_url',
          key: 'image_url',
          ellipsis: {
              showTitle: false,
            },
            render: image_url => (
              <Tooltip placement="topLeft" title={image_url}>
                {image_url}
              </Tooltip>
            ),
      },
        {
            title: 'Year',
            dataIndex: 'year',
            key: 'year',
            sorter: {
                compare: (a, b) => a.year - b.year,
                multiple: 1,
              },
        },
        {
            title: 'Duration',
            dataIndex: 'duration',
            key: 'duration',
            sorter: {
                compare: (a, b) => a.duration - b.duration,
                multiple: 2,
              },
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
        },
        {
            title: 'Rating',
            dataIndex: 'rating',
            key : 'rating',
            sorter: {
                compare: (a, b) => a.rating - b.rating,
                multiple: 3,
              },
        },
        {
            title: 'Review',
            dataIndex: 'review',
            key : 'review',
            ellipsis: {
                showTitle: false,
              },
              render: review => (
                <Tooltip placement="topLeft" title={review}>
                  {review}
                </Tooltip>
              ),
        },
        {
            title: 'Action',
            key: 'action',
            render: (res, index) => (
              <Space size="middle">
                <div key={index}>
                    <button  value={res.id} onClick={handleEdit} className="btn-Edit"><EditOutlined /></button>
                    <button  value={res.id} onClick={handleDelete} className="btn-Delete"><DeleteOutlined /></button>
                </div>
              </Space>
            ),
          },
      ];
      
      const data = dataMovie
      
      
      function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
      }

    return (

        <>
            <div className="table-list">
            <h1>Movie List</h1>
            <h2>Filter Movie</h2>

            <div className="filter">
            <form onSubmit={handleFilter}>
            <Input onChange={handleChangeFilter} value={filter.genre} name="genre" type="text" placeholder="Genre" className="input-filter" allowClear required />
            <Input onChange={handleChangeFilter} value={filter.year} name="year"type="number" placeholder="Year" className="input-filter" allowClear required min={1980} max={2021} />
            <Input onChange={handleChangeFilter} value={filter.rating} name="rating"type="number" placeholder="Rating" className="input-filter" allowClear required min={0} max={10} />
            <Input type="submit" value="Filter" className="reset-filter"/>
            <Button className="reset-filter" onClick={() => {
                    setFetchStatus(true)
                    setFilter({
                      genre: "",
                      year: 0,
                      rating: 0
                    })
                }}>Reset</Button>
            </form>
            </div>
            <Link to="/movie/create" className="add"><Button onClick={handleCreate}>Add New Movie</Button></Link>
            <Space className="search">
            <Search placeholder="Search..." onSearch={onSearch}  allowClear enterButton  /> 
            </Space>
            <Table columns={columns} dataSource={data} onChange={onChange} style={{minHeight : 600}}/>
            </div>
           

        </>

    )
}

export default MovieList