import { Card } from 'antd';
import {useEffect } from 'react';
import { WebContext } from '../Context/Context';
import { useContext } from "react";
import { Link } from 'react-router-dom';
import { Divider } from 'antd';

const { Meta } = Card;

const CardComponent = (props) =>{
    return(
        <>
        <Link to={`/${props.dataName}/${props.id}`}>
        <Card
            hoverable
            style = {{ width: 260 }}
            cover = {<img alt="example" src={props.img} style={{height:300, objectFit:"cover"}}/>}
        >
            <Meta title={props.name} description={props.description}/>
        
        </Card>
        </Link>
        </>
    )
}

const Home = () => {

    const { movieData, gameData, fetchStatus, setFetchStatus, functions} = useContext(WebContext)
    const {fetchMovie, fetchGame, handleText} = functions

    


    useEffect(() => {
        
        if (fetchStatus) {
            fetchMovie()
            fetchGame()
            setFetchStatus(false)
        }
        
    }, [fetchStatus, setFetchStatus]);

    

    return (
        <>
        <div className="home">
        <Divider orientation="left" plain>
            <h1 className="divider">Movies</h1>
        </Divider>
        <div className="card-home">
           {
               movieData !== null && (
                   <>
                    {movieData.filter((res, index) =>{
                        return index < 4
                    }).map((res ,index)=>{
                        return <CardComponent key={index} dataName="movie" id={res.id} img={res.image_url}  name={res.title} description={handleText(res.description, 100)}/>
                    })}     
                   </>
               )
           }
        </div>
        <Divider orientation="left" plain>
            <h1 className="divider">Games</h1>
        </Divider>
        <div className="card-home">
           {
                gameData !== null && (
                   <>
                    {gameData.filter((res, index) =>{
                        return index < 4
                    }).map((res ,index)=>{
                        return <CardComponent key={index} dataName="game" id={res.id} img={res.image_url} name={res.name} description={handleText(res.platform, 100)}/>
                    })}     
                   </>
               )
           }
        </div>
        </div>
        
        </>
    )
}

export default Home