import { useHistory, useParams } from "react-router-dom"
import { Input, Button , Form} from 'antd';
import { useContext, useState } from "react";
import { useEffect } from "react";
import { MovieContext } from "../Context/MovieContext";
import axios from "axios";
import Cookies from "js-cookie";
import { Link } from "react-router-dom";

const MovieForm = () => {

    const {input, setInput, state, functions} = useContext(MovieContext)
    const { setFetchStatus} = state
    const {fetchDatabyID, functionUpdate} = functions

    let history = useHistory()
    let {value} = useParams()

    useEffect(() => {
       if (value !== undefined){
        fetchDatabyID(value)
       }
       }, [])


    const handleSubmit = (event) => {
        event.preventDefault()
        if (value === undefined){
        axios.post(`https://backendexample.sanbersy.com/api/data-movie`, {
            title : input.title,
            genre : input.genre,
            duration : input.duration,
            description : input.description,
            image_url : input.image_url,
            review : input.review,
            year : input.year,
            rating : input.rating,
        },
        {headers:{"authorization" : "bearer" + Cookies.get(`token`)}}
        ).then (() => {
            setFetchStatus(true)
            history.push("/movie")
        })
        } else {
            functionUpdate(value)
            history.push("/movie")
        }
    }

    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name
    
        setInput({ ...input, [name]: value })

    }

    return (
        <div className="game-form">
            <h1>Movies</h1>
        <Form onSubmitCapture={handleSubmit}>
            <label>Title</label>
            <Input type="text" onChange={handleChange} value={input.title} name="title" required/>

            <label>Image</label>
            <Input type="text" onChange={handleChange} value={input.image_url} name="image_url" required/>

            <label>Description</label>
            <Input type="text" onChange={handleChange} value={input.description} name="description" required/>

            <label>Genre</label>
            <Input type="text" onChange={handleChange} value={input.genre} name="genre" required/>

            <label>Duration (Minutes)</label>
            <Input type="number" onChange={handleChange} value={input.duration} name="duration"required min={0}/>

            <label>Year</label>
            <Input type="number" onChange={handleChange} value={input.year} name="year"required min={1980} max={2021}/>

            <label>Rating</label>
            <Input type="number" onChange={handleChange} value={input.rating} name="rating"required min={0} max={10}/>
            
            <label>Review</label>
            <Input type="text" onChange={handleChange} value={input.review} name="review"required/>
            <br /><br /><br />
            <Form.Item>
            <Button type="primary" htmlType="submit" className="form-btn">
            Submit
            </Button>
            <Link to="/movie"><Button className="form-btn">Cancel</Button></Link>
            </Form.Item>
        </Form>
        
        </div>
    );
}

export default MovieForm