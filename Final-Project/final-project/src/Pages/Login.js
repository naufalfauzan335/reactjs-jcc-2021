import { Form, Input, Button, Checkbox,message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { render } from '@testing-library/react';

const LoginForm = () => {
    let history = useHistory()
  const onFinish = (event) => {
    axios.post(`https://backendexample.sanbersy.com/api/user-login`, {
        email : event.email,
        password : event.password
    }) .then((e) => {
        let token = e.data.token
        Cookies.set(`token`, token, {expires : 1})
        history.push("/")
        render(message.success("Loged in"))
    }) .catch((e) => {
        render(message.error("Your email or password are wrong!"))
    })

  };

  return (
      <div className="bg-login">
          <div className="form-login">
          <h1 className="header-form">Login to your account</h1>
        <Form
        name="normal_login"
        className="login-form"
        initialValues={{
            remember: true,
        }}
        onFinish={onFinish}
        method="post"
        >
        <Form.Item
            name="email"
            rules={[
            {
                required: true,
                message: 'Please input your Email!',
            },
            ]}
        >
            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
        </Form.Item>
        <Form.Item
            name="password"
            rules={[
            {
                required: true,
                message: 'Please input your Password!',
            },
            ]}
        >
            <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
            />
        </Form.Item>
        <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
            </Form.Item>
        </Form.Item>

        <Form.Item>
            <br />
            <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
            </Button>
            <br /><br />
            Or <Link to="/register">Register Now</Link>
        </Form.Item>
        </Form>
      </div>
      </div>

  );
};

export default LoginForm