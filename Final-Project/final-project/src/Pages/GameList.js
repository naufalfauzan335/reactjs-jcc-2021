import { Table, Space, Tooltip, Input } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import { GameContext } from '../Context/GameContext';
import {DeleteOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import { useHistory, Link } from 'react-router-dom';
import { Button } from 'antd';
import axios from 'axios';



const GameList = () => {

    const {setInput, functions, state, setDataGame} = useContext(GameContext)
    const {fetchData, functionDelete} = functions
    let {fetchStatus,setFetchStatus, dataGame} = state
    let history = useHistory()
    const { Search } = Input;
    // const [filterStatus, setFilterStatus] = useState([])

    const [filter, setFilter] = useState({
        genre: "",
        release: 0,
        platform: ""
    })

    const handleChangeFilter = (e) => {
        let valueType = e.target.value
        let name = e.target.name

        setFilter({ ...filter, [name] : valueType})
    }

    useEffect(() => {

        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
        }


    }, [fetchData, fetchStatus, setFetchStatus])

    const handleEdit = (event) => {
        let idData = parseInt(event.currentTarget.value)
        history.push(`game/form/${idData}`)
    }
    const handleDelete = (event) => {
        let idData = parseInt(event.currentTarget.value)
        functionDelete(idData)
    }
    const handleCreate = () => {
        history.push("/game/create")
        setInput({
        name : "",
        genre : "",
        platform : "",
        release: 0,
        singlePlayer:true,
        multiplayer:true, 
        image_url:"",
        id:0
        })
    }
    const handleFilter = (e) => {
        e.preventDefault()

        let fetchFilter = async() => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            let data = result.data

            let filterData = data.filter((e) => {
                return e.genre === filter.genre || e.release === filter.release || e.platform === filter.platform
            })
            setDataGame(
                filterData.map((e) => {
                    let {genre,
                        id,
                        image_url,
                        singlePlayer,
                        multiplayer,
                        name,
                        platform,
                        release,
                    } = e
    
                    return {
                        genre,
                        id,
                        image_url,
                        singlePlayer,
                        multiplayer,
                        name,
                        platform,
                        release
                    }
    
                })
                
            )

        }

        fetchFilter()
    }
    const onSearch = value => {
        let fetchSearch = async() => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            let data = result.data

            console.log(data)
            let filterResult = data.filter((e) => {
               return Object.values(e).join("").toLowerCase().includes(value.toLowerCase())
            })
            console.log(filterResult)

            setDataGame(
                filterResult.map((e) => {
                    let {genre,
                        id,
                        image_url,
                        singlePlayer,
                        multiplayer,
                        name,
                        platform,
                        release,
                    } = e
    
                    return {
                        genre,
                        id,
                        image_url,
                        singlePlayer,
                        multiplayer,
                        name,
                        platform,
                        release
                    }
    
                })
                
            )

        }
        fetchSearch()
      };
    const columns = [
        
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Image',
            dataIndex: 'image_url',
            key: 'image_url',
            ellipsis: {
                showTitle: false,
              },
              render: image_url => (
                <Tooltip placement="topLeft" title={image_url}>
                  {image_url}
                </Tooltip>
              ),
            
        },
        {
            title: 'Release',
            dataIndex: 'release',
            key : 'release',
            sorter: {
                compare: (a, b) => a.release - b.release,
                multiple: 1,
              },
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre'
        },
        {
            title: 'Platform',
            key: 'platform',
            dataIndex: 'platform',
        },
        {
            title: 'Single Player',
            dataIndex: 'singlePlayer',
            key: 'singlePlayer'
        },
        {
            title: 'Multi Player',
            dataIndex: 'multiplayer',
            key: 'multiplayer'
        },
        {
            title: 'Action',
            key: 'action',
            render: (res, index) => (
              <Space size="middle">
                <div key={index}>
                    <button  value={res.id} onClick={handleEdit} className="btn-Edit"><EditOutlined /></button>
                    <button  value={res.id} onClick={handleDelete} className="btn-Delete"><DeleteOutlined /></button>
                </div>
              </Space>
            ),
          },
      ];
      
      const data = dataGame
      
      
    return(
        <>
        <div className="table-list">
            <h1>Game List</h1>
            <h2>Filter Game</h2>

        <div className="filter">
        <form onSubmit={handleFilter}>
        <Input onChange={handleChangeFilter} value={filter.genre} name="genre" type="text" placeholder="Genre" className="input-filter" allowClear required />
        <Input onChange={handleChangeFilter} value={filter.release} name="release"type="number" placeholder="Release" className="input-filter" allowClear required min={2000} max={2021}/>
        <Input onChange={handleChangeFilter} value={filter.platform} name="platform"type="text" placeholder="Platform" className="input-filter"allowClear required />
        <Input type="submit" value="Filter" className="reset-filter"/>
        <Button className="reset-filter" onClick={() => {
                setFetchStatus(true)
                setFilter({
                    genre: "",
                    release: 0,
                    platform: "" 
                })
            }}>Reset</Button>
        </form>
        </div>
        <div className="table">
           
        <Link to="/game/create" onClick={handleCreate} className="add"><Button>Add New Game</Button></Link>
        <Space className="search">
        <Search placeholder="Search..." onSearch={onSearch}  allowClear enterButton  /> 
        </Space>
          
        <Table columns={columns} dataSource={data} style={{minHeight : 600}}/>
        </div>
        
        </div>
        
        </>
    )
}

export default GameList