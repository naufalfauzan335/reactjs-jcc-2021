import axios from 'axios';
import { useHistory } from 'react-router-dom';
import {
  Form,
  Input,
  Button,
  message,
} from 'antd';
import { render } from '@testing-library/react'
import Cookies from 'js-cookie';

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const ChangePassword = () => {
  const [form] = Form.useForm();

  let history = useHistory()

  const onFinish = (event) => {
    console.log('Received values of form: ', event);
    axios.post(`https://backendexample.sanbersy.com/api/change-password`, {
        current_password : event.current_password,
        new_password : event.new_password,
        new_confirm_password : event.new_confirm_password
    },{
      headers : {"Authorization" : "Bearer "+ Cookies.get(`token`)}
    }
      ) .then((e) => {
        render(message.success("Your password has been changed!"))
        history.push("/")
    }) .catch((e) => {
        render(message.error(e.response.data))
    })

  };

  
  return (
    <div className="bg-register">
        <div className="form-register">
        <h1 className="header-form">Change Password</h1>
        <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        method="post"
        >
        <Form.Item
            name="current_password"
            label="Current Password"
            rules={[
            {
                required: true,
                message: 'Please input your current password!',
            },
            ]}
        >
            <Input.Password/>
        </Form.Item>

        <Form.Item
            name="new_password"
            label="New Password"
            rules={[
            {
                required: true,
                message: 'Please input your new password!',
            },
            ]}
            hasFeedback
        >
            <Input.Password />
        </Form.Item>

        <Form.Item
            name="new_confirm_password"
            label="Confirm Password"
            dependencies={['new_password']}
            hasFeedback
            rules={[
            {
                required: true,
                message: 'Please confirm your new password!',
            },
            ({ getFieldValue }) => ({
                validator(_, value) {
                if (!value || getFieldValue('new_password') === value) {
                    return Promise.resolve();
                }

                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
            }),
            ]}
        >
            <Input.Password />
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
            Change Password
            </Button>
        </Form.Item>
        </Form>
    </div>
    </div>
    
  );
};

export default ChangePassword