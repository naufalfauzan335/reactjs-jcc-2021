import { useHistory, useParams,Link } from "react-router-dom"
import { Input, Button , Form} from 'antd';
import { useContext, useState } from "react";
import { useEffect } from "react";
import { GameContext } from "../Context/GameContext";
import axios from "axios";
import Cookies from "js-cookie";

const GameForm = () => {

    const {input, setInput, state, functions, currentId, setCurrentId} = useContext(GameContext)
    const { setFetchStatus} = state
    const {fetchDatabyID, functionUpdate} = functions

    let history = useHistory()
    let {valuegame} = useParams()

    useEffect(() => {
       if (valuegame !== undefined){
        fetchDatabyID(valuegame)
       }
    }, [])

      

    const handleSubmit = (event) => {
        event.preventDefault()
        if (valuegame === undefined){
        axios.post(`https://backendexample.sanbersy.com/api/data-game`, {
            genre : input.genre,
            image_url : input.image_url,
            singlePlayer : input.singlePlayer,
            multiplayer : input.multiplayer,
            name : input.name,
            platform : input.platform,
            release : input.release
        },
        {headers:{"authorization" : "bearer" + Cookies.get(`token`)}}
        ).then (() => {
            setFetchStatus(true)
            history.push("/game")
        })
        } else {
            functionUpdate(valuegame)
            history.push("/game")

        }
    }

    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name

        let platform = ["singlePlayer","multiplayer"]

        if(platform.indexOf(name) === -1 ){
            setInput({...input, [name] : value})
        } else {
            setInput({...input, [name] : !input[name]})
        }

    }

    return (
        <div className="game-form">
            <h1>Games</h1>
        <Form onSubmitCapture={handleSubmit}>
            <label>Name</label>
            <Input type="text" onChange={handleChange} value={input.name} name="name" required/>

            <label>Image</label>
            <Input type="text" onChange={handleChange} value={input.image_url} name="image_url" required/>

            <label>Genre</label>
            <Input type="text" onChange={handleChange} value={input.genre} name="genre" required/>

            <label>Platform</label>
            <Input type="text" onChange={handleChange} value={input.platform} name="platform" required/>

            <label>Release</label>
            <Input type="number" onChange={handleChange} value={input.release} name="release" required min ={2000} max={2021} />
            <br /><br />
            <div className="checkbox" style={{display: "flex"}}>
            <label>SinglePlayer</label>
            <Input type="checkbox" className="check-btn" onChange={handleChange} checked={input.singlePlayer} name="singlePlayer" />
            </div>
            <br />
            <div className="checkbox" style={{display: "flex"}}>
            <label >MultiPlayer</label>
            <Input type="checkbox" className="check-btn" onChange={handleChange} checked={input.multiplayer} name="multiplayer"/>
            </div>  
            <br />
            <Form.Item>
            <Button type="primary" htmlType="submit" className="form-btn">
            Submit
            </Button>
            <Link to="/game"><Button className="form-btn">Cancel</Button></Link>
            </Form.Item>

        </Form>
        </div>
    );
}

export default GameForm