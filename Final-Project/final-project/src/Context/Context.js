import axios from "axios"
import React, { createContext, useState } from "react"



export const WebContext = createContext();

export const WebProvider = props => {

    const [movieData, setMovieData] = useState([])
    const [gameData, setGameData] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)
    const [dataMovie, setDataMovie] = useState([])
    const [dataGame, setDataGame] = useState([])
    
    const fetchMovie = async () => {

        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let fetchResult = result.data

        setMovieData(
            fetchResult.map((res) => {
                let {id,title,genre,duration,description,image_url,review,year,rating} = res
                return {
                   id,
                   title,
                   genre,
                   duration,
                   description,
                   image_url,
                   review,
                   year,
                   rating,

                }

            })
        )
    }

   


    const fetchGame = async () => {

        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let fetchResult = result.data
        setGameData(
            fetchResult.map((res) => {
                let {genre,
                    id,
                    image_url,
                    singlePlayer,
                    multiplayer,
                    name,
                    platform,
                    release,
                } = res

                return {
                    genre,
                    id,
                    image_url,
                    singlePlayer,
                    multiplayer,
                    name,
                    platform,
                    release
                }

            })
            
        )
        
    }
    const fetchMoviebyID = async (Id) => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${Id}`)
        let data = result.data
        let {id,title,genre,duration,description,image_url,review,year,rating} = data

        setDataMovie({
            id,
            title,
            genre,
            duration,
            description,
            image_url,
            review,
            year,
            rating
        })
        
    }
    const fetchGamebyID = async (idgame) => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${idgame}`)
        let fetchResult = result.data
        let {genre,id,image_url,singlePlayer,multiplayer, name,platform,release} = fetchResult

        setDataGame({
            genre,
            id,
            image_url,
            singlePlayer,
            multiplayer,
            name,
            platform,
            release
        })
       
    }
    
    const handleText = (text, num) => {
        if(text === null) {
            return ""
        } else {
            return text.slice(text,num) + "..."
        }
    }

    const functions = {
        fetchMovie,
        fetchGame,
        fetchMoviebyID,
        fetchGamebyID,
        handleText
    }

    return (
        <WebContext.Provider value={{
            movieData,
            setMovieData,
            fetchStatus,
            setFetchStatus,
            functions,
            gameData,
            setGameData,
            fetchMoviebyID,
            fetchGamebyID, 
            dataMovie,
            dataGame
          
        }}>
            {props.children}
        </WebContext.Provider>
    )

}