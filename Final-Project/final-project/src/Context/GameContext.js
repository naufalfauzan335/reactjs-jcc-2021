import React, { createContext, useState } from "react"
import axios from "axios"
import Cookies from "js-cookie";
import { useHistory } from "react-router";


export const GameContext = createContext();

export const GameProvider = props => {
    const [fetchStatus, setFetchStatus] = useState(true)
    const [dataGame, setDataGame] = useState([])
    const [currentId, setCurrentId] = useState(null)
    const [input, setInput] = useState({
        name : "",
        genre : "",
        platform : "",
        release: 0,
        singlePlayer:true,
        multiplayer:true, 
        image_url:"",
        id: 0
    })
    let history = useHistory()
    const fetchData = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let data = result.data

        setDataGame(
            data.map((res) => {
                let {genre,
                    id,
                    image_url,
                    singlePlayer,
                    multiplayer,
                    name,
                    platform,
                    release,
                } = res

                return {
                    genre,
                    id,
                    image_url,
                    singlePlayer,
                    multiplayer,
                    name,
                    platform,
                    release
                }

            })
            
        )

    }
    const fetchDatabyID = async (valuegame) => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${valuegame}`)
        let data = result.data
        
        let {genre,
            id,
            image_url,
            singlePlayer,
            multiplayer,
            name,
            platform,
            release,
        } = data
        setInput({
            genre,
            id,
            image_url,
            singlePlayer,
            multiplayer,
            name,
            platform,
            release    
        })
        setCurrentId(data.id)
    }

    const functionSubmit = (valuegame) => {
        axios.post(`https://backendexample.sanbersy.com/api/data-game/${valuegame}`, {
            genre : input.genre,
            image_url : input.image_url,
            singlePlayer : input.singlePlayer,
            multiplayer : input.multiplayer,
            name : input.name,
            platform : input.platform,
            release : input.release,
            id: input.id
        },
        {
            headers:{"authorization" : "bearer" + Cookies.get(`token`)}
        }).then ((event) => {
            setFetchStatus(true)
            history.push("/game")

        })
        
    }
    const functionUpdate = (currentId) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-game/${currentId}`, {
            genre : input.genre,
            image_url : input.image_url,
            singlePlayer : input.singlePlayer,
            multiplayer : input.multiplayer,
            name : input.name,
            platform : input.platform,
            release : input.release,
            id : input.id
        },{
            headers:{"authorization" : "bearer" + Cookies.get(`token`)}
        }).then((event) => {
            setFetchStatus(true)

        }) 
    }

    const functionDelete = (idData) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idData}`, {
            headers:{"authorization" : "bearer" + Cookies.get(`token`)}
        })
        .then(() =>{
            setFetchStatus(true)
        })
    }

    let state = {
        fetchStatus,
        setFetchStatus,
        dataGame, setDataGame
    }
    let functions = {
        fetchData,
        fetchDatabyID,
        functionDelete,
        functionSubmit,
        functionUpdate
    }

    return(
        <GameContext.Provider value={{
            functions,
            state,
            input,
            setInput,
            currentId,
            setCurrentId,
            setDataGame,
            dataGame
            
        }}>
            {props.children}
        </GameContext.Provider>
    )
}