import React, { createContext, useState } from "react"
import axios from "axios"
import Cookies from "js-cookie";


export const MovieContext = createContext();

export const MovieProvider = props => {
    const [fetchStatus, setFetchStatus] = useState(true)
    const [dataMovie, setDataMovie] = useState([])
    const [input, setInput] = useState({
        title : "",
        genre : "",
        duration: 0,
        description : "",
        image_url: "",
        review: "",
        year : 0,
        rating : 0,
        id:0
    })
    const fetchData = async() => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let data = result.data

        setDataMovie(
            data.map((res) => {
                let {id,title,genre,duration,description,image_url,review,year,rating} = res
                return {
                   id,
                   title,
                   genre,
                   duration,
                   description,
                   image_url,
                   review,
                   year,
                   rating,

                }

            })
        )

    }

    const fetchDatabyID = async (value) => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${value}`)
        let data = result.data
        let {id,title,genre,duration,description,image_url,review,year,rating} = data
        setInput({
            id,
            title,
            genre,
            duration,
            description,
            image_url,
            review,
            year,
            rating,    
        })
    }
    const functionUpdate = (currentId) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-movie/${currentId}`, {
            id : input.id,
            title : input.title,
            genre : input.genre ,
            duration : input.duration,
            description : input.description,
            image_url : input.image_url,
            review : input.review,
            year : input.year,
            rating : input.rating,
        },{
            headers:{"authorization" : "bearer" + Cookies.get(`token`)}
        }).then((event) => {
            setFetchStatus(true)

        }) 
    }
    const functionDelete = (idData) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idData}`, {
            headers:{"authorization" : "bearer" + Cookies.get(`token`)}
        })
        .then(() =>{
            setFetchStatus(true)
        })
    }

    let state = {
        fetchStatus,
        setFetchStatus,
        dataMovie, setDataMovie
    }
    let functions = {
        fetchData,
        functionDelete,
        fetchDatabyID,
        functionUpdate
    }

    return(
        <MovieContext.Provider value={{
            functions,
            state,
            input,
            setInput,
            dataMovie,
            setDataMovie
        }}>
            {props.children}
        </MovieContext.Provider>
    )
}