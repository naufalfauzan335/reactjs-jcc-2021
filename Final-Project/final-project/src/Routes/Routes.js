import React from "react";
import LayoutComponent from "../Layout/LayoutComponent";
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";
import Home from "../Pages/Home";
import Login from "../Pages/Login";
import MovieList from "../Pages/Movielist";
import { WebProvider } from "../Context/Context";
import Cookies from "js-cookie";
import { Redirect } from "react-router-dom"; 
import RegisterForm from "../Pages/Register";
import ChangePassword from "../Pages/ChangePassword";
import MovieDetail from "../Pages/MovieDetail";
import GameDetail from "../Pages/GameDetail"
import GameList from "../Pages/GameList";
import { GameProvider } from "../Context/GameContext";
import GameForm from "../Pages/GameForm";
import { MovieProvider } from "../Context/MovieContext";
import MovieForm from "../Pages/MovieForm";

const Routes = () => {

    const RouteLogin = ({...props}) => {
        if (Cookies.get(`token`) === undefined ){
           return <Route {...props}/>
        } else if (Cookies.get(`token`) !== undefined){
           return <Redirect to="/"/> 
        }
    }

    const PrivateRoute = ({...props}) => {
        if (Cookies.get(`token`) === undefined){
            return <Redirect to="/login"/>
        } else if (Cookies.get(`token`) !== undefined){
            return <Route {...props}/>
        }
    }

    return(
        <>
        

        
        <WebProvider>
            <MovieProvider>
            <GameProvider>
            <Router>
                <Switch>
                    <Route exact path='/'>
                        {<LayoutComponent content={<Home/>}/>}
                    </Route>

                    <RouteLogin exact path='/login'>
                        {<LayoutComponent content={<Login/>}/>}
                    </RouteLogin>

                    <RouteLogin exact path='/register'>
                        {<LayoutComponent content={<RegisterForm/>}/>}
                    </RouteLogin>

                    <PrivateRoute exact path='/change-password'>
                        {<LayoutComponent content={<ChangePassword/>}/>}
                    </PrivateRoute>
                    
                    <PrivateRoute exact path='/game'>
                        {<LayoutComponent content={<GameList/>}/>}
                    </PrivateRoute>
                    <PrivateRoute exact path='/game/form'>
                        {<LayoutComponent content={<GameForm/>}/>}
                    </PrivateRoute>
                    <PrivateRoute exact path='/game/form/:valuegame'>
                        {<LayoutComponent content={<GameForm/>}/>}
                    </PrivateRoute>

                    <PrivateRoute exact path='/game/create'>
                        {<LayoutComponent content={<GameForm/>}/>}
                    </PrivateRoute>
                    
                    <PrivateRoute exact path='/movie/create'>
                        {<LayoutComponent content={<MovieForm/>}/>}
                    </PrivateRoute>    

                    <Route exact path='/movie/:Id'>
                        {<LayoutComponent content={<MovieDetail/>}/>}
                    </Route>

                    <Route exact path='/game/:idgame'>
                        {<LayoutComponent content={<GameDetail/>}/>}
                    </Route>
                    
                    <PrivateRoute exact path='/movie'>
                        {<LayoutComponent content={<MovieList/>}/>}
                    </PrivateRoute>
                    <PrivateRoute exact path='/movie/form'>
                        {<LayoutComponent content={<MovieForm/>}/>}
                    </PrivateRoute>
                    <PrivateRoute exact path='/movie/form/:value'>
                        {<LayoutComponent content={<MovieForm/>}/>}
                    </PrivateRoute>

                </Switch>
            </Router>
            </GameProvider>
            </MovieProvider>
            
        </WebProvider>           
        </>
    )
}

export default Routes