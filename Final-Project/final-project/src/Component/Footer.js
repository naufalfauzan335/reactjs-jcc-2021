import React from "react"


const Footer = () => {
    return (
        <>
            <footer>
                <h5>Copyright &copy; 2021 M.Naufal Fauzan Q. </h5>
            </footer>
        </>
    )
}

export default Footer