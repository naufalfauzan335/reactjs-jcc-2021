import React from "react";
import { Layout, Menu} from 'antd';
import { SettingFilled, FormOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";


const { SubMenu } = Menu;
const { Sider } = Layout;


const Sidebar = () => {
    return (
        <>
            <Sider width={200} className="site-layout-background" >
                <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%', borderRight: 0, }}
                >
                <SubMenu key="sub1" icon={<FormOutlined />} title="Editor">
                    <Menu.Item key="1"><Link to='/game'>Games</Link></Menu.Item>
                    <Menu.Item key="2"><Link to='/movie'>Movies</Link></Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<SettingFilled/>} title="Settings">
                    <Menu.Item key="3"><Link to='/change-password'>Change Password</Link></Menu.Item>
                </SubMenu>
                
                </Menu>
            </Sider>
        </>
    )
}

export default Sidebar