import {Layout, Menu} from 'antd';
import Logo from '../Assets/img/logo.png'
import React from "react";
import { Link, useHistory } from 'react-router-dom';
import Cookies from 'js-cookie';


const {Header} = Layout;

const Navbar = () => {

    let history = useHistory()

    const handleLogout = () => {
        Cookies.remove(`token`)
        history.push("/login")
    }
 
    return (
        <>
        <Header className="header">
            
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']} > 
                
                <Menu.Item key="3"><Link to='/'>Home</Link></Menu.Item>
                {
                    Cookies.get(`token`) === undefined ? <>
                        <Menu.Item key="0" style={{position: 'absolute', right: 20}}><Link to='/login'>Login</Link></Menu.Item>
                        <Menu.Item key="1" style={{position: 'absolute', right: 20, marginRight : 80}}><Link to='/register'>Register</Link></Menu.Item> 
                    </> : null
                }
                {
                    Cookies.get(`token`) !== undefined ? <Menu.Item onClick={handleLogout} key="2" style={{position: 'absolute', right: 20}}>Logout</Menu.Item> : null
                }
                    
            </Menu>

        </Header>
        </>
    )
}
export default Navbar