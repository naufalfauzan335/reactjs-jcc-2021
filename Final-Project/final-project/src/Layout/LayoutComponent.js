import { Layout } from 'antd';
import Navbar from '../Component/Navbar';
import Footer from '../Component/Footer';
import Sidebar from '../Component/Sidebar';
import Cookies from 'js-cookie';


const { Content } = Layout;

const LayoutComponent = (props) => {
    return (
        
        <>
        <Layout>
            <Navbar/>
            <Layout>
                {Cookies.get(`token`) !== undefined ? <Sidebar/> : null}
                <Layout>
                    <Content className="site-layout-background" >
                        {props.content}
                    </Content>
                </Layout>
            </Layout>
        </Layout>
        <Footer/>
        </>
    )
}

export default LayoutComponent